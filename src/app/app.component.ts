import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSelectChange } from "@angular/material/select";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'select_menu_test';
  toppings = new FormControl('');
  var1=false;
  @ViewChild('targetToDisable') selectElement:any;

  selectedData: { value: string; text: string } = {
    value: "",
    text: ""
  };

  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  selectedValue(event: MatSelectChange) {
    this.selectedData = {
      value: event.value,
      text: event.source.triggerValue
    };
    console.log(this.selectedData);
    // event.source.disabled=true; // Here the select component gets disabled (event.source > the select component)
    this.var1=true; // Here the select component gets disabled by realtime variable
  }
  ngAfterViewInit() {
    // ElementRef { nativeElement: <input> }
    console.log(this.selectElement.disabled);
  }
  reset(){
    if(this.selectElement.disabled){
      // this.selectElement.disabled=false;
      this.var1=false;
      console.log(this.selectElement.disabled);
    }
  }
}

